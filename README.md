# iOS Application

iOS Application called SUPLI is a project made for the course ISIS3510 in the period 202010 in Universidad de Los Andes in Bogotá Colombia. This is an Application to manage product prices, in the specific scenario of the coronavirus situation.

### Requirements

To run this application, because it is an iOS Application, you need to have a macOS device, there are different options. Also, you need XCode 11.0 as a minimum. It is not necessary to have a physical iPhone device, it can be run in the xCode emulator.

## How to run the project

1. clone the repository in your local machine
2. deployed the app in XCode
3. With the use of terminal go to the main file and run the command "pod install" (make sure that you already have a pod in your mac)
4. Run the emulator(We advise you to use iPhone 11 pro, however it will work with whatever iPhone you use)
5. If you want to login directly just use the username: salomon@gmail.com and the password: Uniandes2020
6. Navigate through the app!

## Authors

Camilo Beltran and Santiago Saenz
