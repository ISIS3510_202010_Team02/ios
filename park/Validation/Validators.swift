//
//  Validators.swift
//  park
//
//  Created by Camilo Beltran on 27/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import Foundation

class ValidationError: Error {
    var message: String
    
    init(_ message: String) {
        self.message = message
    }
}

protocol ValidatorConvertible {
    func validated(_ value: String) throws -> String
}

enum ValidatorType {
    case email
    case password
    case cedula
    case department
    case requiredField(field: String)
    case name
}

enum VaildatorFactory {
    static func validatorFor(type: ValidatorType) -> ValidatorConvertible {
        switch type {
        case .email: return EmailValidator()
        case .password: return PasswordValidator()
        case .cedula: return CedulaValidator()
        case .department: return DepartmentValidator()
        case .requiredField(let fieldName): return RequiredFieldValidator(fieldName)
        case .name: return NameValidator()
        }
    }
}

struct EmailValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Correo Invalido")
            }
        } catch {
            throw ValidationError("Correo Invalido")
        }
        return value
    }
}

struct PasswordValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Se requiere una contraseña")}
        guard value.count >= 8 else { throw ValidationError("La contraseña debe ser de al menos 8 caracteres") }
        
        do {
            if try NSRegularExpression(pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("La contraseña debe ser de al menos 8 caracteres, con al menos una letra y un numero")
            }
        } catch {
            throw ValidationError("La contraseña debe ser de al menos 8 caracteres, con al menos una letra y un numero")
        }
        return value
    }
}

//"J3-123A" i.e
struct CedulaValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        do {
            if try NSRegularExpression(pattern: "^[0-9]{1,15}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Cedula no valida")
            }
        } catch {
            throw ValidationError("Cedula no valida")
        }
        return value
    }
}

struct DepartmentValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value.count >= 3 else {
            throw ValidationError("La ciudad debe tener más de tres caracteres" )
        }
        guard value.count < 30 else {
            throw ValidationError("La ciudad no debe contener más de 30 caracteres" )
        }
        
        do {
            if try NSRegularExpression(pattern: "^[a-z]{1,30}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Ciudad invalida, no se debe incluir espacios, numeros ni caracteres especiales")
            }
        } catch {
            throw ValidationError("Ciudad invalida, no se debe incluir espacios, numeros ni caracteres especiales")
        }
        return value
    }
}

struct RequiredFieldValidator: ValidatorConvertible {
    private let fieldName: String
    
    init(_ field: String) {
        fieldName = field
    }
    
    func validated(_ value: String) throws -> String {
        guard !value.isEmpty else {
            throw ValidationError("Campo " + fieldName + " requerido")
        }
        return value
    }
}

struct NameValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value.count >= 3 else {
            throw ValidationError("El nombre debe tener más de tres caracteres" )
        }
        guard value.count < 40 else {
            throw ValidationError("El nombre no debe contener más de 40 caracteres" )
        }
        
        do {
            if try NSRegularExpression(pattern: "^\\w{3,40}",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Ciudad invalida, no se debe incluir espacios, numeros ni caracteres especiales")
            }
        } catch {
            throw ValidationError("Ciudad invalida, no se debe incluir espacios, numeros ni caracteres especiales")
        }
        return value
    }
}




