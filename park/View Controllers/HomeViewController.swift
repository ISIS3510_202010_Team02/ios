//
//  HomeViewController.swift
//  park
//
//  Created by Santiago Sàenz on 4/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import Foundation

class HomeViewController: UIViewController {
    
    @IBOutlet weak var nombre: UILabel!
    
    var ref = Database.database().reference()
    
    
    override func viewDidLoad() {
        
        let userID : String = (Auth.auth().currentUser?.email)!
        print("Current user ID is" + userID)
        
        
        self.nombre.text = "Hola, "
        let Arr = userID.components(separatedBy: "@")
        self.nombre.text! += Arr[0]
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if Checkinternet.Connection() == false{
            
            self.Alert(Message: "You do not have an internet connection")
            
        }
        
    }
    
    func Alert (Message: String){
        
        let alert = UIAlertController(title: "Alert", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    
    
}
