//
//  Information.swift
//  park
//
//  Created by Santiago Sàenz on 26/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import Foundation
import Foundation
import UIKit
import FirebaseAuth
import Firebase
import Foundation
import FirebaseFirestore

class Information: UIViewController {
    

    
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var precio: UILabel!
    @IBOutlet weak var tienda: UILabel!
    
    @IBOutlet weak var Imagen: UIImageView!
    
    @IBOutlet weak var articulos: UIPickerView!
    
    
    
    var text:String = ""
    
    var db: Firestore!
    private var dataImg: [String] = []
    private var dataPrecio: [String] = []
    private var dataTienda: [String] = []
     private var dataNombre: [String] = []
    private var dataSource: [String] = ["Producto1","Producto2","Producto3","Producto4","Producto5","Producto6","Producto7","Producto8","Producto9","Producto10",]
    
    override func loadView() {
        // [START setup]
        
                      let settings = FirestoreSettings()
                      
                      Firestore.firestore().settings = settings
                      // [END setup]
                      db = Firestore.firestore()
              
             
              
               db.collection("productos2").getDocuments() { (querySnapshot, err) in
                   if let err = err {
                       print("Error getting documents: \(err)")
                   } else {
                       for document in querySnapshot!.documents {
                        
                        self.dataImg.append(document.data()["img"] as! String)
                        self.dataPrecio.append(String(document.data()["price"] as! Double))
                        self.dataNombre.append(document.data()["name"] as! String)
                        
                        self.dataTienda.append(document.data()["store"] as! String)
                          
                       }

                   }
               }
        
        
        print(self.dataSource.count)
        print(self.dataImg.count)
        print(self.dataPrecio.count)
       
        super.loadView()
    }
    
    override func viewDidLoad() {
        //categoria.text? = text
      articulos.dataSource = self
             articulos.delegate = self
        super.viewDidLoad()
       
        
    }
   
    
    override func viewDidAppear(_ animated: Bool) {
                    
                    if Checkinternet.Connection() == false{
                        
                        self.Alert(Message: "Your Device is not connected with internet")
                        
                    }
                    
                }
                
                func Alert (Message: String){
                    
                 let alert = UIAlertController(title: "Alert", message: Message, preferredStyle: UIAlertController.Style.alert)
                 alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
    
    func setImage(from url: String) {
     
        guard let imageURL = URL(string: url) else { return }

            // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }

            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.Imagen.image = image
            }
        }
    }
    

}

extension Information: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
             return 1
         }
         
         func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
             return dataSource.count
         }
         
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return dataSource[row]
         }
         
         func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            nombre.text = dataNombre[row]
            precio.text = dataPrecio[row]
            tienda.text = dataTienda[row]
            setImage(from: dataImg[row])
            
         }
}


