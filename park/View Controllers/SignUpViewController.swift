//
//  SignUpViewController.swift
//  park
//
//  Created by Santiago Sàenz on 4/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var usuario: UITextField!
    @IBOutlet weak var clave: UITextField!
    @IBOutlet weak var cedula: UITextField!
    @IBOutlet weak var ciudad: UITextField!
    @IBOutlet weak var nombre: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUpElements()
        
        usuario.layer.cornerRadius = 22.0
        usuario.layer.borderWidth = 0
        usuario.layer.masksToBounds = true
        usuario.clipsToBounds = true
        
        clave.layer.cornerRadius = 22.0
        clave.layer.borderWidth = 0
        clave.layer.masksToBounds = true
        clave.clipsToBounds = true
        
        cedula.layer.cornerRadius = 22.0
        cedula.layer.borderWidth = 0
        cedula.layer.masksToBounds = true
        cedula.clipsToBounds = true
        
        ciudad.layer.cornerRadius = 22.0
        ciudad.layer.borderWidth = 0
        ciudad.layer.masksToBounds = true
        ciudad.clipsToBounds = true
        
        nombre.layer.cornerRadius = 22.0
        nombre.layer.borderWidth = 0
        nombre.layer.masksToBounds = true
        nombre.clipsToBounds = true
        
        registerButton.layer.cornerRadius = 22.0
        registerButton.layer.borderWidth = 0
        registerButton.layer.masksToBounds = true
        registerButton.clipsToBounds = true
        
    }
    
    func setUpElements(){
        
    }
    
    func validateFields() -> String{
        
        return "Por favor llene todos los campos"
    }
    
    @IBAction func register(_ sender: Any) {
        
        do {
            let usu = try usuario.validatedText(validationType: ValidatorType.email)
            let clav = try clave.validatedText(validationType: ValidatorType.password)
            let ced = try cedula.validatedText(validationType: ValidatorType.cedula)
            let ciud = try ciudad.validatedText(validationType: ValidatorType.department)
            let nom = try nombre.validatedText(validationType: ValidatorType.name)
            
            Auth.auth().createUser(withEmail: usu, password: clav) { (result, err) in
                
                let db = Firestore.firestore()
                db.collection("users").addDocument(data: ["usuario" : usu,"cedula":ced ,"ciudad":ciud, "nombre": nom]) { (error) in
                    if error != nil {
                        
                    }
                    else{
                        
                        let homeViewController = self.storyboard?.instantiateViewController(identifier: "HomeVC") as? HomeViewController
                        
                        self.view.window?.rootViewController = homeViewController
                        self.view.window?.makeKeyAndVisible()
                    }
                }
            }
            
        } catch(let error) {
            self.Alert(Message: (error as! ValidationError).message)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if Checkinternet.Connection() == false{
            self.Alert(Message: "Your Device is not connected with internet")
        }
        
    }
    
    func Alert (Message: String){
        
        let alert = UIAlertController(title: "Error", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
