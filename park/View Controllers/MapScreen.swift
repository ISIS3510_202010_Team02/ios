//
//  MapScreen.swift
//  park
//
//  Created by Santiago Sàenz on 18/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class MapScreen: UIViewController , MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 10000
    let annotation1 = MKPointAnnotation()
    let annotation2 = MKPointAnnotation()
    let annotation3 = MKPointAnnotation()
    let annotation4 = MKPointAnnotation()

    override func viewDidLoad() {
        
        
        annotation1.coordinate = CLLocationCoordinate2D(latitude: 37.798493, longitude: -122.431890)
        annotation1.title = "Carulla"
        annotation1.subtitle = "Existen 20 personas cerca al área"
        
        mapView.addAnnotation(annotation1)
        
        annotation2.coordinate = CLLocationCoordinate2D(latitude: 37.736246, longitude: -122.392093)
        annotation2.title = "Éxito"
        annotation2.subtitle = "Existen 40 personas cerca al área"
        mapView.addAnnotation(annotation2)
        
        annotation3.coordinate = CLLocationCoordinate2D(latitude: 37.769883, longitude: -122.417073)
        annotation3.title = "D1"
        annotation3.subtitle = "Existen 5 personas cerca al área"
        mapView.addAnnotation(annotation3)
        
        annotation4.coordinate = CLLocationCoordinate2D(latitude: 37.798728, longitude: -122.399922)
        annotation4.title = "Ara"
        annotation4.subtitle = "Existen 15 personas cerca al área"
        mapView.addAnnotation(annotation4)
        
        super.viewDidLoad()
        checkLocationServices()
    }
    
    func setupLocationManager(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func checkLocationServices(){
        
        if CLLocationManager.locationServicesEnabled(){
            //set up our location manager
            setupLocationManager()
            checkLocationAuthorization()
        }else{
            //show alert letting the user know they have to turn this on
        }
    }
    
    func centerViewOnUserLocation(){
        if let location = locationManager.location?.coordinate{
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus(){
        case .authorizedWhenInUse:
            // do map stuff
            mapView.showsUserLocation = true
            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            break
        case .denied:
            //show alert instructing them how to turn the permissions
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            //show an alert letting know what is up
            break
        case .authorizedAlways:
            break
        @unknown default:
           break
        }
    }

}

extension MapScreen: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //we´ll be back
        guard let location = locations.last else { return }
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion.init(center: center, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        mapView.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //we´ll be back
        checkLocationAuthorization()
    }
    
    override func viewDidAppear(_ animated: Bool) {
                 
                 if Checkinternet.Connection() == false{
                     
                     self.Alert(Message: "Your Device is not connected with internet")
                     
                 }
                 
                 
             }
             
             func Alert (Message: String){
                 
              let alert = UIAlertController(title: "Alert", message: Message, preferredStyle: UIAlertController.Style.alert)
              alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
                 self.present(alert, animated: true, completion: nil)
                 
                 
             }
    
}
