//
//  Validation.swift
//  park
//
//  Created by Camilo Beltran on 27/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import Foundation

class Validation {
    func isEmailValid(_ value: String) -> Bool {
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                return false
            }
        } catch {
            return false
        }
        return true
    }
}
