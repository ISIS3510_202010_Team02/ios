//
//  ProductViewController.swift
//  park
//
//  Created by Santiago Sàenz on 4/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import Foundation
import FirebaseFirestore

class ProductViewController: UIViewController {
    
    
    @IBOutlet weak var Producto1: UILabel!
    
    @IBOutlet weak var Valor1: UITextField!
    
    @IBOutlet weak var Producto2: UILabel!
    
    @IBOutlet weak var Valor2: UITextField!
    
    @IBOutlet weak var Producto3: UILabel!
    
    @IBOutlet weak var Valor3: UITextField!
    
    @IBOutlet weak var Producto4: UILabel!
    
    @IBOutlet weak var Valor4: UITextField!
    
    @IBOutlet weak var Producto5: UILabel!
    
    @IBOutlet weak var Valor5: UITextField!
    
    var db: Firestore!
    
    override func viewDidLoad() {
       
         // [START setup]
               let settings = FirestoreSettings()
               
               Firestore.firestore().settings = settings
               // [END setup]
               db = Firestore.firestore()
               
              
        db.collection("productos").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                var cont = 1
                for document in querySnapshot!.documents {
                    
                   
                  
                    if (cont == 1){
                        let nombreProducto = document.data()["name"] as! String
                        self.Producto1.text! = nombreProducto.components(separatedBy: " ")[0]
                        let num1 = document.data()["price"] as! Double
                        self.Valor1.text! = String(num1)
                    }else if( cont == 2){
                        let nombreProducto = document.data()["name"] as! String
                        self.Producto2.text! = nombreProducto.components(separatedBy: " ")[0]
                        let num2 = document.data()["price"] as! Double
                        self.Valor2.text! = String(num2)
                    }
                        else if( cont == 3){
                            let nombreProducto = document.data()["name"] as! String
                            self.Producto3.text! = nombreProducto.components(separatedBy: " ")[0]
                            let num2 = document.data()["price"] as! Double
                            self.Valor3.text! = String(num2)
                        }
                        else if( cont == 4){
                            let nombreProducto = document.data()["name"] as! String
                            self.Producto4.text! = nombreProducto.components(separatedBy: " ")[0]
                            let num2 = document.data()["price"] as! Double
                            self.Valor4.text! = String(num2)
                        }
                    else{
                        let nombreProducto = document.data()["name"] as! String
                        self.Producto5.text! = nombreProducto.components(separatedBy: " ")[0]
                        let num3 = document.data()["price"] as! Double
                        self.Valor5.text! = String(num3)
                    }

                    cont += 1
                    print("\(document.documentID) => \(document.data())")
                    print(document.data()["name"] as! String)
                    print(document.data()["price"] as! Double)
                    

                }

            }
        }
   
        // Do any additional setup after loading the view, typically from a nib.
         super.viewDidLoad()
       
      
    }

    override func viewDidAppear(_ animated: Bool) {
                 
                 if Checkinternet.Connection() == false{
                     
                     self.Alert(Message: "Your Device is not connected with internet")
                     
                 }
                 
             }
             
             func Alert (Message: String){
                 
              let alert = UIAlertController(title: "Alert", message: Message, preferredStyle: UIAlertController.Style.alert)
              alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
                 self.present(alert, animated: true, completion: nil)
                 
                 
             }
    
}
