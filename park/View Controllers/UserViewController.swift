//
//  UserViewController.swift
//  park
//
//  Created by Santiago Sàenz on 4/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import Foundation
import FirebaseFirestore

class UserViewController: UIViewController {
    
    
    @IBOutlet weak var nombre: UITextField!
    @IBOutlet weak var apellido: UITextField!
    @IBOutlet weak var cedula: UITextField!
    @IBOutlet weak var correo: UITextField!
    
    var db: Firestore!
    
    override func viewDidLoad() {
        
        nombre.layer.cornerRadius = 22.0
        nombre.layer.borderWidth = 0
        nombre.layer.masksToBounds = true
        nombre.clipsToBounds = true
        
        apellido.layer.cornerRadius = 22.0
        apellido.layer.borderWidth = 0
        apellido.layer.masksToBounds = true
        apellido.clipsToBounds = true
        
        cedula.layer.cornerRadius = 22.0
        cedula.layer.borderWidth = 0
        cedula.layer.masksToBounds = true
        cedula.clipsToBounds = true
        
        correo.layer.cornerRadius = 22.0
        correo.layer.borderWidth = 0
        correo.layer.masksToBounds = true
        correo.clipsToBounds = true
        
        // [START setup]
        let settings = FirestoreSettings()
        
        Firestore.firestore().settings = settings
        // [END setup]
        db = Firestore.firestore()
        
        let correo : String = (Auth.auth().currentUser?.email)!
        
        
        db.collection("users").whereField("usuario", isEqualTo: correo)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        print("\(document.documentID) => \(document.data())")
                        let ArrS = document.data()["nombre"] as! String
                            
                         let Arr = ArrS.components(separatedBy: " ")
                                       self.nombre.text! = Arr[0]
                                       self.apellido.text! = Arr[1]
                        self.cedula.text! = document.data()["cedula"] as! String
                        
                    }
                }
        }
        
 
        
        self.correo.text = correo
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
                 
                 if Checkinternet.Connection() == false{
                     
                     self.Alert(Message: "Your Device is not connected with internet")
                     
                 }
                 
             }
             
             func Alert (Message: String){
                 
              let alert = UIAlertController(title: "Alert", message: Message, preferredStyle: UIAlertController.Style.alert)
              alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
                 self.present(alert, animated: true, completion: nil)
                 
                 
             }
    
}
