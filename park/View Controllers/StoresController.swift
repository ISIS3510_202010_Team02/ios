//
//  StoresController.swift
//  park
//
//  Created by Santiago Sàenz on 26/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth
import Firebase
import Foundation
import FirebaseFirestore

class StoresController: UIViewController {


    @IBOutlet weak var imagenTienda: UIImageView!
    @IBOutlet weak var horario: UILabel!
    
    @IBOutlet weak var tiendas: UIPickerView!
    
    private let dataSource = ["Éxito","Carulla","Tiendas Ara"]
    private var dataImg = [] as [String]
    private var dataHorario = [] as [String]
    
    
   
    
    
     var db: Firestore!
    
    
    override func viewDidLoad() {
        
          // [START setup]
                let settings = FirestoreSettings()
                
                Firestore.firestore().settings = settings
                // [END setup]
                db = Firestore.firestore()
                
               
        tiendas.dataSource = self
        tiendas.delegate = self
        
        
        
         db.collection("stores").getDocuments() { (querySnapshot, err) in
             if let err = err {
                 print("Error getting documents: \(err)")
             } else {
                 for document in querySnapshot!.documents {
                     let horarioProducto = document.data()["schedule"] as! String
                     let imagenProducto = document.data()["img"] as! String
                    self.dataImg.append(imagenProducto)
                    self.dataHorario.append(horarioProducto)
                    
                    
                 }

             }
         }
    
         // Do any additional setup after loading the view, typically from a nib.
          super.viewDidLoad()
     }

    func setImage(from url: String) {
     
        guard let imageURL = URL(string: url) else { return }

            // just not to cause a deadlock in UI!
        DispatchQueue.global().async {
            guard let imageData = try? Data(contentsOf: imageURL) else { return }

            let image = UIImage(data: imageData)
            DispatchQueue.main.async {
                self.imagenTienda.image = image
            }
        }
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
                    
                    if Checkinternet.Connection() == false{
                        
                        self.Alert(Message: "Your Device is not connected with internet")
                        
                    }
                    
                }
                
                func Alert (Message: String){
                    
                 let alert = UIAlertController(title: "Alert", message: Message, preferredStyle: UIAlertController.Style.alert)
                 alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
    
}

extension StoresController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
             return 1
         }
         
         func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
             return dataSource.count
         }
         
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return dataSource[row]
         }
         
         func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
             horario.text = dataHorario[row]
            setImage(from: dataImg[row])
            
         }
}

