//
//  LogInViewController.swift
//  park
//
//  Created by Santiago Sàenz on 4/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class LogInViewController: UIViewController {
    
    @IBOutlet var usuario: UITextField!
    @IBOutlet var clave: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    private let titleFont = UIFont.boldSystemFont(ofSize: 30)
    private let textFieldFont = UIFont.systemFont(ofSize: 16)
    private let buttonFont = UIFont.boldSystemFont(ofSize: 20)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        usuario.layer.cornerRadius = 22.0
        usuario.layer.borderWidth = 0
        usuario.layer.masksToBounds = true
        usuario.clipsToBounds = true
        
        clave.layer.cornerRadius = 22.0
        clave.layer.borderWidth = 0
        clave.layer.masksToBounds = true
        clave.clipsToBounds = true
        
        loginButton.layer.cornerRadius = 22.0
        loginButton.layer.borderWidth = 0
        loginButton.layer.masksToBounds = true
        loginButton.clipsToBounds = true
        
        registerButton.layer.cornerRadius = 22.0
        registerButton.layer.borderWidth = 0
        registerButton.layer.masksToBounds = true
        registerButton.clipsToBounds = true
        
    }
    
    @IBAction func loginButt(_ sender: Any) {
        
        let us = usuario.text!
        let cla = clave.text!
        
        if(!isEmailValid(us)) {
            self.Alert(Message: "Ingrese un correo valido.")
        }
        
        Auth.auth().signIn(withEmail: us, password: cla) { (result, error) in
            
            if error != nil{
                self.Alert(Message: "La clave o el ususario no es correcto")
            }
            else{
                
                let homeViewController = self.storyboard?.instantiateViewController(identifier: "HomeVC") as? HomeViewController
                
                self.view.window?.rootViewController = homeViewController
                self.view.window?.makeKeyAndVisible()
                
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if Checkinternet.Connection() == false{
            self.Alert(Message: "Your Device is not connected with internet")
        }
        
    }
    
    func Alert (Message: String){
        
        let alert = UIAlertController(title: "Alert", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func isEmailValid(_ value: String) -> Bool {
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                return false
            }
        } catch {
            return false
        }
        return true
    }
    
}
