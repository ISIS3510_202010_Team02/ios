//
//  UITextField+Extensions.swift
//  park
//
//  Created by Camilo Beltran on 27/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import UIKit.UITextField

extension UITextField {
    func validatedText(validationType: ValidatorType) throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType)
        return try validator.validated(self.text!)
    }
}
