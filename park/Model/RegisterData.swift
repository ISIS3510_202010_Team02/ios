//
//  RegisterData.swift
//  park
//
//  Created by Camilo Beltran on 27/04/20.
//  Copyright © 2020 Andes University. All rights reserved.
//

import Foundation

struct RegisterData {
    var email: String
    var password: String
    var cedula: String
    var department: String
    var name: String
}
